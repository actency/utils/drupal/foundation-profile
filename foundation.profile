<?php

/**
 * Implements hook_install_tasks_alter().
 */
function foundation_install_tasks_alter(&$tasks, $install_state) {
  unset($tasks['install_config_download_translations']);
  unset($tasks['install_import_translations']);
  unset($tasks['install_finish_translations']);
}
